"""Save movie to rosbag2."""

from pytube import YouTube     # downloading an example movie
import numpy as np             # image data array transformation
import cv2                     # examplary image processing
import os, datetime            # timestamp generation

from rosbags.rosbag2 import Writer
from rosbags.serde import serialize_cdr
from rosbags.typesys.types import builtin_interfaces__msg__Time as Time
from rosbags.typesys.types import sensor_msgs__msg__CompressedImage as CompressedImage
from rosbags.typesys.types import std_msgs__msg__Header as Header

TOPIC = '/camera/compressed'  # "compressed" movies have to be labeled 
                              # with a specific sub topic
FRAMEID = 'global'
BAGFILE = 'rosbag'
MOVIEFILE = 'rosbag_tutorial.mp4'


def save_movie(initial_timestamp: datetime) -> None:
    """Iterate over all IMAGES of a movie and save them to ros bag."""
    writer = Writer(BAGFILE)
    writer.open()
    conn = writer.add_connection(TOPIC, CompressedImage.__msgtype__, 'cdr', '')

    cap = cv2.VideoCapture(MOVIEFILE)
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    count = 0

    while(cap.isOpened()):
        ret, in_image = cap.read()
        if ret:
            # just for illustrating opencv processing 
            adapted_image = cv2.rotate(in_image, cv2.ROTATE_180)

            time_stamp_nano =  int((initial_timestamp +
                                    datetime.timedelta(0, 
                                                       1/fps * count)).timestamp() * 1000000000)
            message = CompressedImage(
                Header(
                    stamp=Time(
                        sec=time_stamp_nano // 1000000000,
                        nanosec=time_stamp_nano % 1000000000
                    ),
                    frame_id='world',
                ),
                format='rgb8; jpeg compressed bgr8',
                # Transform shape of cv2 image
                # http://wiki.ros.org/rospy_tutorials/Tutorials/WritingImagePublisherSubscriber
                # Line 86
                data = np.array(cv2.imencode('.jpeg', adapted_image)[1])
            )
            writer.write(conn, time_stamp_nano, 
                         serialize_cdr(message, CompressedImage.__msgtype__))
            count+=1
            if count > 5 * fps:
                break
    writer.close()

if __name__ == '__main__':
    # download rosbag tutorial from ros wiki
    movie_url = 'https://youtu.be/pwlbArh_neU'   
    youtube = YouTube(movie_url)
    my_video = youtube.streams.filter(res="720p").first()
    my_video.download("", MOVIEFILE)

    # get timestamp from file
    c_time = os.path.getctime(MOVIEFILE)
    initial_timestamp = datetime.datetime.fromtimestamp(c_time)

    # run transformation
    save_movie(initial_timestamp)

